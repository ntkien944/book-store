<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
	<p>Upload status: <%= request.getAttribute("message") %></p>
	<a href="<c:url value="/admin"/>">Go back</a>
	<p>Refesh admin page to load new uploaded image</p>
</body>
</html>