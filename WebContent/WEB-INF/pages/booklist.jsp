<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page import="java.util.List"%>
<%@ page import="ltm.model.Book"%>
<%@ page import="ltm.model.Category"%>

<%
	List<Book> booklist = (List<Book>) request.getAttribute("booklist");
	List<Category> catglist = (List<Category>) request.getAttribute("catglist");
%>
<h3>Book list</h3>
<script>
var catg_info = new Array();
<% for (int i = 0; i < catglist.size(); i++){
	Category catg = catglist.get(i);%>
	catg_info["k<%=catg.getID() %>"] = "<%=catg.getName() %>";
<%}%>
function book_save(index){
	var contents = $('#book-' + index).find('td');
    var contentArray = 'bookID=' + contents[0].innerHTML;
    contentArray = contentArray + '&category=' + $('.book-' + index + '-catg').val();
    
   	if(contents[2].innerHTML.length != 0) 
   		contentArray = contentArray + '&name=' + trim_string(contents[2].innerHTML);
   	if(contents[3].innerHTML.length != 0) 
   		contentArray = contentArray + '&author=' + trim_string(contents[3].innerHTML);
   	
   	if(contents[5].innerHTML.length != 0) 
   		contentArray = contentArray + '&publisher=' + trim_string(contents[5].innerHTML);
   	if(contents[6].innerHTML.length != 0) 
   		contentArray = contentArray + '&price=' + trim_string(contents[6].innerHTML);
   	
    $.ajax({
    	type: "POST",
        data: contentArray,
        url: "<c:url value="/admintask?mode=bsave"/>",
        success: function(response)
        {   
        	if(response.message == 'OK'){
        		// nothing
        	}else{
        		alert('save failed');
        	}
        }
    });
    
}
function book_remove(index){
	var contents = $('#book-' + index).find('td');
    var contentArray = 'bookID=' + contents[0].innerHTML;
	$.ajax({
    	type: "POST",
    	data: contentArray,
        url: "<c:url value="/admintask?mode=bremove"/>",
        success: function(response)
        {   
        	if(response.message == 'OK'){
        		$('#book-' + index).remove();
        	}else{
        		alert('remove failed');
        	}
        }
    });
}
function book_add(){
	$.ajax({
    	type: "POST",
        url: "<c:url value="/admintask?mode=badd"/>",
        success: function(response)
        {   
        	if(response.message == '-1'){
        		alert('add failed');
        	}else{
        		// push new row
        		var html = '<tr id="book-' + response.message + '"><td>' + response.message + '</td>'
        		+ '<td><img src="' + response.message + '.jpg"</td>'
    			+ '<td contenteditable="true"></td>'
    			+ '<td contenteditable="true"></td>'
    			+ '<td><select id="book_select_id" class="book-' + response.message + '-catg">';
    			for (var k in catg_info){
    				html = html + '<option value="' + k.substr(1) + '">' + catg_info[k] + '</option>';
    			}
    			html = html + '</select></td><td contenteditable="true"></td><td contenteditable="true">0</td>'
    			+ '<td><button type="button" class="save_btn" onclick="book_save('
    			+ response.message + ')">save</button><button type="button" onclick="book_remove('
    			+ response.message + ')">remove</button></td></tr>';
    			
    			// push
    			$('#book_list').append(html);
        	}
        }
    });
}
</script>
<div class="table-container" style="overflow:auto;">
	<table class="table" id="book_list">
		<tr>
			<th>ID</th>
			<th>Image</th>
			<th>Name</th>
			<th>Author</th>
			<th>Category</th>
			<th>Publisher</th>
			<th>Price</th>
			<th>action</th>
		</tr>
		<%
			for (int i = 0; i < booklist.size(); i++) {
				Book book = booklist.get(i);
				Category catg = null;
				if (book.getCategory() == null){
					catg = catglist.get(0);
				}else{
					int k = book.getCategory();
					int h;
					for(h = 0; h < catglist.size(); h++){
						catg = catglist.get(h);
						if ((catglist.get(h)).getID() == k) break;
						
					}
					if(h >= catglist.size()) catg = catglist.get(0);
				}
				String bookimg = request.getContextPath() + "/files/" + book.getBookID() + ".jpg";
		%>
		<tr id="book-<%=book.getBookID()%>">
			<td><%=book.getBookID()%></td>
			<td><img height="100" src="<%= bookimg %>"/></td>
			<td contenteditable="true"><%=book.getName()%></td>
			<td contenteditable="true"><%=book.getAuthor()%></td>
			<td>
				<select id="book_select_id" class="book-<%=book.getBookID()%>-catg">
				<% for (int j = 0; j < catglist.size(); j++){
					   Category catgitem = catglist.get(j);
				%>
					<option
					<% if (catgitem.getID() == catg.getID()){%>
						selected="selected"
					<%}%>
					value="<%= catgitem.getID() %>"><%= catgitem.getName() %></option>
				<% } %>
				</select>
			</td>
			<td contenteditable="true"><%=book.getPublisher()%></td>
			<td contenteditable="true"><%=book.getPrice()%></td>
			<td>
				<button type="button" class="save_btn" onclick="book_save(<%=book.getBookID() %>)">save</button>
				<button type="button" onclick="book_remove(<%=book.getBookID() %>)">remove</button>
			</td>
		</tr>
		<%
			}
		%>
	</table>
</div>
<div class="book_command">
	<button type="button" onclick="book_add()">add new book</button>
</div>
