<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page import="java.util.List"%>
<%@ page import="ltm.model.Category"%>

<%
	List<Category> catglist = (List<Category>) request.getAttribute("catglist");
%>
<div style="padding: 10px;">
	<script>
	function catg_save(index){
		var contents = $('#catg-' + index).find('td');
	    var contentArray = 'id=' + contents[0].innerHTML;
	    
	   	if(contents[1].innerHTML.length != 0) 
	   		contentArray = contentArray + '&name=' + trim_string(contents[1].innerHTML);
	   	
	    $.ajax({
	    	type: "POST",
	        data: contentArray,
	        url: "<c:url value="/admintask?mode=csave"/>",
	        success: function(response)
	        {   
	        	if(response.message == 'OK'){
	        		location.assign(location.href);
	        	}else{
	        		alert('save failed');
	        	}
	        }
	    });
	    
	}
	function catg_remove(index){
		var contents = $('#catg-' + index).find('td');
	    var contentArray = 'id=' + contents[0].innerHTML;
		$.ajax({
	    	type: "POST",
	    	data: contentArray,
	        url: "<c:url value="/admintask?mode=cremove"/>",
	        success: function(response)
	        {   
	        	if(response.message == 'OK'){
	        		location.assign(location.href);
	        	}else{
	        		alert('remove failed');
	        	}
	        }
	    });
	}
	function catg_add(){
		$.ajax({
	    	type: "POST",
	        url: "<c:url value="/admintask?mode=cadd"/>",
	        success: function(response)
	        {   
	        	if(response.message == '-1'){
	        		alert('add failed');
	        	}else{
	        		// push new row
	        		var html = '<tr id="catg-' + response.message + '"><td>' + response.message + '</td>'
	    			+ '<td contenteditable="true"></td><td>'
	    			+ '<button type="button" onclick="catg_save('
	    			+ response.message + ')">save</button><button type="button" onclick="catg_remove('
	    			+ response.message + ')">remove</button></td></tr>';
	    			
	    			// push
	    			$('#catg_list').append(html);
	        	}
	        }
	    });
	}
	</script>
	<table class="table" id="catg_list">
		
		<%
			for (int i = 0; i < catglist.size(); i++) {
				Category catg = catglist.get(i);
		%>
		<tr id="catg-<%=catg.getID()%>">
			<td><%=catg.getID()%></td>
			<td contenteditable="true"><%=catg.getName()%></td>
			<td>
				<button type="button" onclick="catg_save(<%=catg.getID() %>)">save</button>
				<button type="button" onclick="catg_remove(<%=catg.getID() %>)">remove</button>
			</td>
		</tr>
		<%
			}
		%>
	</table>
	<div class="category_command">
		<button type="button" onclick="catg_add()">add new category</button>
	</div>
</div>
<div class="clearfix"></div>