<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Bookstore admin</title>
<link rel="stylesheet" href="<c:url value="/css/bootstrap.css"/>">
<link rel="stylesheet" href="<c:url value="/css/style.css"/>">
</head>
<body>
	<script src="<c:url value="/js/jquery.js"/>"></script>
	<script src="<c:url value="/js/bootstrap.min.js"/>"></script>
	<script>
	function trim_string(str){
		var s = str.substr(str.length - 4);
		if (s == '<br>'){
			return str.substr(0, str.length - 4);
		}else{
			return str;
		}
	}
	</script>
	<h2>Welcome, administrator</h2>
	<div class="panel-group col-md-3">
		<div class="panel panel-default">
			<div class="panel-heading">
				<a data-toggle="collapse" href="#collapseOne">
          		Upload file</a>
			</div>
			<div id="collapseOne" class="panel-collapse collapse out">
				<jsp:include page="uploadfile.jsp" />
			</div>
		</div>
	</div>
	<div class="panel-group col-md-6">
		<div class="panel panel-default">
			<div class="panel-heading">
				<a data-toggle="collapse"
          			href="#collapseTwo">
          		Category</a>
			</div>
			<div class="panel-collapse collapse out" id="collapseTwo">
				<jsp:include page="categorylist.jsp" />
			</div>
		</div>
	</div>
	<div class="clearfix"></div>
	<jsp:include page="booklist.jsp" />


	<div class="clearfix"></div>
	<div class="col-md-6 col-md-offset-3 textcenter">LTM - DNA - NTK
	</div>
</body>
</html>