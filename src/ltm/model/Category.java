package ltm.model;

import java.io.Serializable;

public class Category implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Integer ID;
	private String Name;
	
	public Integer getID() {
		return ID;
	}
	public void setID(Integer categoryID) {
		this.ID = categoryID;
	}
	public String getName() {
		return Name;
	}
	public void setName(String name) {
		Name = name;
	}
	
	
}
