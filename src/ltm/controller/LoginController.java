package ltm.controller;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.Servlet;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ltm.utils.ErrorMessage;
import ltm.utils.HibernateUtils;

/**
 * 
 */
// @WebServlet(description = "Login servlet: check username ans password", urlPatterns = { "/Login" })
public class LoginController implements Servlet {
	
	private static final Logger logger = LoggerFactory.getLogger(LoginController.class);
    ServletConfig config = null;

	@Override
	public void destroy() {
		
	}

	@Override
	public ServletConfig getServletConfig() {
		return config;
	}

	@Override
	public String getServletInfo() {
		return "LoginController, copytight@ 2007-2010";
	}

	@Override
	public void service(ServletRequest req, ServletResponse res) throws ServletException, IOException {
		String username = req.getParameter("user");
		String password = req.getParameter("pass");
		
		logger.info("Processing login...");
		res.setContentType("text/html");
		res.setCharacterEncoding("utf-8");
		PrintWriter out = res.getWriter();
		
		if( username == null || password == null){
			out.print(ErrorMessage.msg_WrongParam);
		}else{
			SessionFactory sessionFactory = HibernateUtils.getSessionFactory();
			Session session = sessionFactory.getCurrentSession();
			Transaction tx = session.beginTransaction();
			String hql = "SELECT Username FROM User WHERE Username = :username AND Password = :password";
			Query query = session.createQuery(hql)
					.setParameter("username", username)
					.setParameter("password",  password);
			
			
			if(query.list().size() > 0){
				logger.info("User " + username + " login succeed");
				out.print(ErrorMessage.msg_Success);
			}else{
				logger.info("User " + username + " login failed");
				out.print(ErrorMessage.msg_Failed);
			}
			tx.commit();
		}
	}

	@Override
	public void init(ServletConfig config) throws ServletException {
		this.config = config;
		logger.info("Login Controller loaded.");
	}
}