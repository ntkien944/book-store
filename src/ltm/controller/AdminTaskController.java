package ltm.controller;

import java.io.IOException;
import java.io.PrintWriter;


import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ltm.dao.BookDAO;
import ltm.dao.CategoryDAO;
import ltm.model.Book;
import ltm.model.Category;
import ltm.utils.ErrorMessage;

@WebServlet(description = "Administration Task", urlPatterns = { "/admintask" })
public class AdminTaskController extends HttpServlet{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private static final Logger logger = LoggerFactory.getLogger(AdminTaskController.class);
	private BookDAO bookDAO = new BookDAO();
	private CategoryDAO catgDAO = new CategoryDAO();
	
	@Override 
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		logger.info("Processing GET request");
		
		resp.setCharacterEncoding("utf-8");
		resp.setContentType("application/json");
		PrintWriter out = resp.getWriter();
		
		out.print("{\"message\":\"");
		// print result here
		out.print("OK");
		
		out.print("\"}");
	}
	
	@Override 
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		logger.info("Processing POST request");
		
		resp.setCharacterEncoding("utf-8");
		resp.setContentType("application/json");
		PrintWriter out = resp.getWriter();
		
		out.print("{\"message\":\"");
		
		// process the request
		String mode = req.getParameter("mode");
		if(mode == null){
			out.print(ErrorMessage.msg_FAIL);
		}else{
			logger.info("mode = " + mode);
			Book book = new Book();
			Category catg = new Category();
			switch(mode){
			case "bsave":
				try{
					book.setBookID(Integer.parseInt(req.getParameter("bookID")));
					book.setName(req.getParameter("name"));
					book.setAuthor(req.getParameter("author"));
					book.setCategory(Integer.parseInt(req.getParameter("category")));
					book.setPublisher(req.getParameter("publisher"));
					book.setPrice(Integer.parseInt(req.getParameter("price")));
					
					logger.info("Saving book...");
					if(bookDAO.updateBook(book) == true){
						out.print(ErrorMessage.msg_OK);
					}else{
						out.print(ErrorMessage.msg_FAIL);
					}
				}catch(NumberFormatException e){
					out.print(ErrorMessage.msg_FAIL);
				}
				break;
			case "bremove":
				try{
					book.setBookID(Integer.parseInt(req.getParameter("bookID")));
					if (bookDAO.removeBook(book) == true)
						out.print(ErrorMessage.msg_OK);
					else
						out.print(ErrorMessage.msg_FAIL);
				}catch(NumberFormatException e){
					out.print(ErrorMessage.msg_FAIL);
				}
				break;
			case "badd":
				int newBookID = bookDAO.addBook(book);
				out.print(newBookID);
				break;
				
				
			case "cadd":
				int newCatgID = catgDAO.addCategory(catg);
				out.print(newCatgID);
				break;
			case "csave":
				try{
					catg.setID(Integer.parseInt(req.getParameter("id")));
					catg.setName(req.getParameter("name"));
					
					logger.info("Saving category...");
					if(catgDAO.updateCategory(catg) == true){
						out.print(ErrorMessage.msg_OK);
					}else{
						out.print(ErrorMessage.msg_FAIL);
					}
				}catch(NumberFormatException e){
					out.print(ErrorMessage.msg_FAIL);
				}
				break;
			case "cremove":
				try{
					catg.setID(Integer.parseInt(req.getParameter("id")));
					if (catgDAO.removeCategory(catg) == true)
						out.print(ErrorMessage.msg_OK);
					else
						out.print(ErrorMessage.msg_FAIL);
				}catch(NumberFormatException e){
					out.print(ErrorMessage.msg_FAIL);
				}
				break;
			default:
				out.print(ErrorMessage.msg_FAIL);
				return;
			}
		}
		
		out.print("\"}");
	}
}
