package ltm.controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import javax.servlet.Servlet;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.gson.Gson;

import ltm.model.User;
import ltm.utils.ErrorMessage;
import ltm.utils.HibernateUtils;

// @WebServlet(description = "User information controller", urlPatterns = { "/User" })
public class UserController implements Servlet{
	
	private static final Logger logger = LoggerFactory.getLogger(UserController.class);
    ServletConfig config = null;
    
	@Override
	public void destroy() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public ServletConfig getServletConfig() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getServletInfo() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void init(ServletConfig config) throws ServletException {
		this.config = config;
		logger.info("User Controller loaded.");
	}

	@Override
	public void service(ServletRequest req, ServletResponse res) throws ServletException, IOException {
		res.setContentType("text/html");
		res.setCharacterEncoding("utf-8");
		PrintWriter out = res.getWriter();
		
		String mode = req.getParameter("mode");
		switch(mode){
		case "1": // get user information, return JSON
			res.setContentType("application/json");
			out.print(getUserInfo(req));
			break;
		case "2": // update user information
			out.print(updateUserInfo(req));
			break;
		case "3": // register new user
			out.print(addNewUser(req));
			break;
		default:
			out.print(ErrorMessage.msg_BadRequest);
		}
	}
	
	private String getUserInfo(ServletRequest req){
		logger.info("Getting user info");
		// return JSON object
		String username = req.getParameter("user");
		if(username == null){
			return ErrorMessage.msg_WrongParam;
		}
		
		SessionFactory sessionFactory = HibernateUtils.getSessionFactory();
		Session session = sessionFactory.getCurrentSession();
		Transaction tx = session.beginTransaction();
		String hql = "FROM User WHERE Username = :username";
		Query query = session.createQuery(hql)
				.setParameter("username", username);
		
		@SuppressWarnings("unchecked")
		List<User> lst = query.list();
		tx.commit();
		if(lst.size() > 0){
			User user = lst.get(0);
			Gson gson = new Gson();
			return gson.toJson(user);
		}else{
			return ErrorMessage.msg_NotExist;
		}
	}
	
	private String updateUserInfo(ServletRequest req){
		logger.info("updating user info");
		String username = req.getParameter("user");
		String password = req.getParameter("pass");
		String newpass = req.getParameter("newpass");
		
		if(username == null){
			return ErrorMessage.msg_WrongParam;
		}
		
		if(newpass == null || newpass.length() == 0){
			return ErrorMessage.msg_PasvEmpty;
		}
		
		SessionFactory sessionFactory = HibernateUtils.getSessionFactory();
		Session session = sessionFactory.getCurrentSession();
		Transaction tx = session.beginTransaction();
		String hql = "FROM User WHERE Username = :username";
		Query query = session.createQuery(hql)
				.setParameter("username", username);
		
		@SuppressWarnings("unchecked")
		List<User> lst = query.list();
		if(lst.size() > 0){
			User cruser = lst.get(0);
			if(cruser.getPassword().equals(password)){
				cruser.setPassword(newpass);
				cruser.setEmail(req.getParameter("email"));
				cruser.setAddress(req.getParameter("address"));
				cruser.setPhone(req.getParameter("phone"));
				
				int credit = 0;
				try{
					credit = Integer.parseInt(req.getParameter("credit"));
				}catch(NumberFormatException e){
					credit = 0;
				}
				cruser.setCredit(credit);
				session.saveOrUpdate(cruser);
				tx.commit();
				return ErrorMessage.msg_Success;
			}else{
				return ErrorMessage.msg_PasvValidation;
			}
		}else{
			return ErrorMessage.msg_NotExist;
		}
	}
	
	private String addNewUser(ServletRequest req){
		logger.info("Processing adding new user...");
		String username = req.getParameter("user");
		String password = req.getParameter("pass");
		
		if(username == null){
			return ErrorMessage.msg_WrongParam;
		}
		
		if(password == null || password.length() == 0){
			return ErrorMessage.msg_PasvEmpty;
		}
		
		SessionFactory sessionFactory = HibernateUtils.getSessionFactory();
		Session session = sessionFactory.getCurrentSession();
		Transaction tx = session.beginTransaction();
		String hql = "FROM User WHERE Username = :username";
		Query query = session.createQuery(hql)
				.setParameter("username", username);
		
		@SuppressWarnings("unchecked")
		List<User> lst = query.list();
		if(lst.size() == 0){
			User user = new User();
			user.setUsername(username);
			user.setPassword(password);
			user.setEmail(req.getParameter("email"));
			user.setAddress(req.getParameter("address"));
			user.setPhone(req.getParameter("phone"));
			session.saveOrUpdate(user);
			tx.commit();
			return ErrorMessage.msg_Success;
		}else{
			return ErrorMessage.msg_UserExist;
		}
	}
}
