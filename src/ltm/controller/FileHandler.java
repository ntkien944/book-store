package ltm.controller;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.tomcat.util.http.fileupload.FileItemIterator;
import org.apache.tomcat.util.http.fileupload.FileItemStream;
import org.apache.tomcat.util.http.fileupload.disk.DiskFileItemFactory;
import org.apache.tomcat.util.http.fileupload.servlet.ServletFileUpload;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ltm.utils.ErrorMessage;

public class FileHandler extends HttpServlet {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private static final Logger logger = LoggerFactory.getLogger(FileHandler.class);
	
	private boolean isMultipart;
	private String filePath;
	private int maxFileSize = 1024 * 1024; // 1MB max
	private int maxMemSize = 4 * 1024;

	public void init() {
		// Get the file location where it would be stored.
		filePath = "/files/";
	}

	public void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, java.io.IOException {
		// Check that we have a file upload request
		isMultipart = ServletFileUpload.isMultipartContent(request);
		String msg = ErrorMessage.msg_FAIL;
		
		if (!isMultipart) {
			logger.info("ERROR: need to specify multipart mode");
		}else{
			DiskFileItemFactory factory = new DiskFileItemFactory();
			// maximum size that will be stored in memory
			factory.setSizeThreshold(maxMemSize);
			// Location to save data that is larger than maxMemSize.
			factory.setRepository(new File("\temp"));
	
			// Create a new file upload handler
			ServletFileUpload upload = new ServletFileUpload(factory);
			// maximum file size to be uploaded.
			upload.setSizeMax(maxFileSize);
	
			try {
				// Process the uploaded file items
				FileItemIterator i = upload.getItemIterator(request);
				FileItemStream fi;
				String filename = null;
				do {
					if(i.hasNext()){
						fi = i.next();
						if(fi.getFieldName().equals("serverfilename")){
							// get the content
							StringBuffer sbuf = new StringBuffer();
							InputStream is = new BufferedInputStream(fi.openStream());
							int sdata = -1;
							while((sdata = is.read()) != -1){
								sbuf.append((char)sdata);
							}
							
							if(sbuf.length() == 0){
								logger.info("ERROR: empty destination filename");
								break;
							}
							sbuf.append(".jpg");
							filename = sbuf.toString();
							logger.info("Destination filename: " + filename);
						}else{
							logger.info("ERROR: field name: serverfilename not found!");
							break;
						}
					}else{
						logger.info("ERROR: iterator empty");
						break;
					}
					if (i.hasNext()) {
						fi = i.next();
						logger.info("FileItem: " + fi.getName());
						// if (!fi.isFormField()) {
						// Write the file
						if (filename == null || fi.getName() == null || fi.getName().length() == 0) {
							logger.info("ERROR: filename not set");
							break;
						}
						
						InputStream is = new BufferedInputStream(fi.openStream());
						String absoluteFilePath = getServletContext().getRealPath(filePath + filename);
						logger.info("Full file path: " + absoluteFilePath);
						
						File file = new File(absoluteFilePath);
						if(!file.exists()) file.createNewFile();
						
						FileOutputStream output = new FileOutputStream(file);
						int data = -1;
						while((data = is.read()) != -1){
							output.write(data);
						}
						is.close();
						output.close();
					}
					msg = ErrorMessage.msg_OK;
				} while(false); // so we can break out easily
				
			} catch (Exception ex) {
				logger.info("ERROR: exception raised");
				ex.printStackTrace();
			}
		} // multi-part
		logger.info("Dispatching to JSP file...");
		request.setAttribute("message", msg);
		request.getRequestDispatcher("/WEB-INF/pages/upload_result.jsp").forward(request, response);
	}

	public void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, java.io.IOException {

		response.setContentType("text");
		PrintWriter out = response.getWriter();
		out.print("Not supported.");
	}
}
