package ltm.controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import javax.servlet.Servlet;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.gson.Gson;

import ltm.model.Book;
import ltm.utils.HibernateUtils;
import ltm.utils.ErrorMessage;

// @WebServlet(description = "Provides book information", urlPatterns = { "/Book" })
public class BookController implements Servlet{

	private static final Logger logger = LoggerFactory.getLogger(BookController.class);
	ServletConfig config = null;
	@Override
	public void destroy() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public ServletConfig getServletConfig() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getServletInfo() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void init(ServletConfig config) throws ServletException {
		this.config = config;
		logger.info("Book Controller loaded.");
	}

	@Override
	public void service(ServletRequest req, ServletResponse res) throws ServletException, IOException {
		res.setContentType("text/html");
		res.setCharacterEncoding("utf-8");
		PrintWriter out = res.getWriter();
		
		String result = null;
		String id = req.getParameter("id");
		if(id == null){
			String catg = req.getParameter("catg");
			if(catg == null){
				out.print(ErrorMessage.msg_WrongParam);
				return;
			}else{
				// check cateID
				int cate = 0;
				try{
					cate = Integer.parseInt(catg);
				}catch(NumberFormatException e){
					out.print(ErrorMessage.msg_WrongParam);
					return;
				}
				result = getBookByCatg(cate);
				if(result != null){
					res.setContentType("application/json");
					out.print(result);
				}else{
					out.print(ErrorMessage.msg_NotExist);
				}
			}
		}else{
			// check BookID
			int uid = 0;
			try{
				uid = Integer.parseInt(id);
			}catch(NumberFormatException e){
				out.print(ErrorMessage.msg_WrongParam);
				return;
			}
			result = getBookByID(uid);
			if(result != null){
				res.setContentType("application/json");
				out.print(result);
			}else{
				out.print(ErrorMessage.msg_NotExist);
			}
			
		}
	}
	
	private String getBookByID(int id){
		SessionFactory sessionFactory = HibernateUtils.getSessionFactory();
		Session session = sessionFactory.getCurrentSession();
		Transaction tx = session.beginTransaction();
		
		String hql = "FROM Book WHERE BookID = :id";
		Query query = session.createQuery(hql)
				.setParameter("id", id);
		@SuppressWarnings("unchecked")
		List<Book> lst = query.list();
		tx.commit();
		if(lst.size() > 0){
			String json = null;
			Gson gson = new Gson();
			json = gson.toJson(lst.get(0));
			return json;
		}else{
			return null;
		}
	}
	
	private String getBookByCatg(int cateID){
		SessionFactory sessionFactory = HibernateUtils.getSessionFactory();
		Session session = sessionFactory.getCurrentSession();
		Transaction tx = session.beginTransaction();
		Query query = null;
		if(cateID == 0){
			String hql = "FROM Book";
			query = session.createQuery(hql);
		}
		else{
			String hql = "FROM Book WHERE Category = :cateid";
			query = session.createQuery(hql)
					.setParameter("cateid", cateID);
		}
		@SuppressWarnings("unchecked")
		List<Book> lst = query.list();
		tx.commit();
		if(lst.size() > 0){
			String json = null;
			Gson gson = new Gson();
			json = gson.toJson(lst);
			return json;
		}else{
			return null;
		}
	}
}
