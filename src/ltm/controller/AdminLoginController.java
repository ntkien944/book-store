package ltm.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ltm.dao.BookDAO;
import ltm.dao.CategoryDAO;
import ltm.model.Book;
import ltm.model.Category;
import ltm.utils.HibernateUtils;

@WebServlet(description = "Administration", urlPatterns = { "/admin" })
public class AdminLoginController extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private static final Logger logger = LoggerFactory.getLogger(AdminLoginController.class);
	
	private BookDAO bookDAO = new BookDAO();
	private CategoryDAO catgDAO = new CategoryDAO();
	
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
	{
		String pass = request.getParameter("pass");
		
		if(pass == null){
			logger.info("No password sent, fordward to login page");
			request.setAttribute("message", "Enter password to continue");
			try {
				request.getRequestDispatcher("/WEB-INF/pages/login.jsp").forward(request, response);
			} catch (ServletException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}else{
			// we have a password, so now times to check
			SessionFactory sessionFactory = HibernateUtils.getSessionFactory();
			Session session = sessionFactory.getCurrentSession();
			Transaction tx = session.beginTransaction();
			Query q = session.createQuery("SELECT Password FROM User WHERE Username = 'admin'");
			String realpass = (String)q.uniqueResult();
			tx.commit();
			if(realpass.equals(pass)){
				Cookie cookie = new Cookie("user", "loggedin");
				cookie.setMaxAge(-1);
				cookie.setHttpOnly(true);
				response.addCookie(cookie);
				
				List<Book> booklist = bookDAO.getBooks();
				List<Category> catglist = catgDAO.getCategories();
				request.setAttribute("booklist", booklist);
				request.setAttribute("catglist", catglist);
				logger.info("OK, welcome");
				try {
					request.getRequestDispatcher("/WEB-INF/pages/admin_home.jsp").forward(request, response);
				} catch (ServletException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}else{
				logger.info("Password mismatch, fordward to login page");
				request.setAttribute("message", "Password mismatch, enter again");
				try {
					request.getRequestDispatcher("/WEB-INF/pages/login.jsp").forward(request, response);
				} catch (ServletException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		} // pass check
	}
	
	@Override
	public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		boolean logged = false;
		Cookie[] cookies = request.getCookies();
		if (cookies != null){
			for(Cookie cookie : cookies){
				if(cookie.getValue().equals("loggedin")){
					logged = true;
					break;
				}
			}
		}
		
		if(logged == false){
			logger.info("No password sent, fordward to login page");
			request.setAttribute("message", "Enter password to continue");
			try {
				request.getRequestDispatcher("/WEB-INF/pages/login.jsp").forward(request, response);
			} catch (ServletException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}else{
			List<Book> booklist = bookDAO.getBooks();
			List<Category> catglist = catgDAO.getCategories();
			request.setAttribute("booklist", booklist);
			request.setAttribute("catglist", catglist);
			logger.info("OK, welcome");
			try {
				request.getRequestDispatcher("/WEB-INF/pages/admin_home.jsp").forward(request, response);
			} catch (ServletException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
}
