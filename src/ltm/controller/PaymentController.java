package ltm.controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import javax.servlet.Servlet;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ltm.model.Cart;
import ltm.utils.ErrorMessage;
import ltm.utils.HibernateUtils;

// @WebServlet(description = "Control user payment", urlPatterns = { "/Payment" })
public class PaymentController implements Servlet {

	private static final Logger logger = LoggerFactory.getLogger(PaymentController.class);
	ServletConfig config = null;
	@Override
	public void destroy() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public ServletConfig getServletConfig() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getServletInfo() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void init(ServletConfig config) throws ServletException {
		this.config = config;
		logger.info("Payment Controller loaded.");
	}

	@Override
	public void service(ServletRequest req, ServletResponse res) throws ServletException, IOException {
		res.setContentType("text/html");
		res.setCharacterEncoding("utf-8");
		PrintWriter out = res.getWriter();
		
		String username = req.getParameter("user");
		String value = req.getParameter("value");
		
		if(username == null || value == null){
			out.print(ErrorMessage.msg_WrongParam);
			return;
		}
		
		int ival = 0;
		try{
			ival = Integer.parseInt(value);
		}catch(NumberFormatException e){
			out.print(ErrorMessage.msg_WrongParam);
			return;
		}
		String result = processPayment(username, ival);
		out.print(result);
	}
	
	private String processPayment(String username, int ival){
		
		logger.info("Processing payment!");
		SessionFactory sessionFactory = HibernateUtils.getSessionFactory();
		Session session = sessionFactory.getCurrentSession();
		Transaction tx = session.beginTransaction();
		String hql = "FROM Cart WHERE Username = :username";
		Query query = session.createQuery(hql)
				.setParameter("username", username);
		
		@SuppressWarnings("unchecked")
		List<Cart> lstCart = query.list();
		int totalPaid = 0;
		if(lstCart.size() > 0){
			// calc the real value
			Cart cart;
			for(int i = 0; i < lstCart.size(); i++){
				cart = lstCart.get(i);
				Query q = session.createQuery("SELECT Price FROM Book WHERE BookID = :id")
						.setParameter("id", cart.getBookID());
				if(q.uniqueResult() == null){
					logger.info("Book not exist");
					return ErrorMessage.msg_NotExist;
				}
				int price = (Integer)q.uniqueResult();
				if(price == 0){
					logger.info("Got price = 0");
					return ErrorMessage.msg_NotExist;
				}else{
					totalPaid = totalPaid + price * cart.getQuantity();
				}
			}
			if(totalPaid == ival){
				// clear cart
				Query q = session.createQuery("DELETE FROM Cart WHERE Username = :user")
						.setParameter("user", username);
				q.executeUpdate();
				tx.commit();
				//
				return ErrorMessage.msg_Success;
			}else{
				logger.info("Payment failed, wrong value of money");
				return ErrorMessage.msg_Failed;
			}
		}else{
			logger.info("Payment failed, Cart empty");
			return ErrorMessage.msg_NotExist;
		}
	}

}
