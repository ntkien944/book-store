package ltm.controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import javax.servlet.Servlet;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.gson.Gson;

import ltm.utils.HibernateUtils;
import ltm.model.Category;

// @WebServlet(description = "return list of category", urlPatterns = { "/Catg" })
public class CategoryController implements Servlet {

	private static final Logger logger = LoggerFactory.getLogger(CategoryController.class);
	ServletConfig config;
	@Override
	public void destroy() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public ServletConfig getServletConfig() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getServletInfo() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void init(ServletConfig config) throws ServletException {
		this.config = config;
		logger.info("Category Controller loaded.");
	}

	@Override
	public void service(ServletRequest req, ServletResponse res) throws ServletException, IOException {
		res.setContentType("application/json");
		res.setCharacterEncoding("utf-8");
		PrintWriter out = res.getWriter();
		
		Session session = HibernateUtils.getSessionFactory().getCurrentSession();
		Transaction tx = session.beginTransaction();
		Query q = session.createQuery("from Category");
		
		@SuppressWarnings("unchecked")
		List<Category> lst = q.list();
		Gson gson = new Gson();
		String result = gson.toJson(lst);
		out.print(result);
		tx.commit();
	}

}
