package ltm.controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import javax.servlet.Servlet;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ltm.model.Cart;
import ltm.utils.ErrorMessage;
import ltm.utils.HibernateUtils;

// @WebServlet(description = "Control user cart", urlPatterns = { "/Cart" })
public class CartController implements Servlet{

	private static final Logger logger = LoggerFactory.getLogger(CartController.class);
	ServletConfig config = null;
	
	@Override
	public void destroy() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public ServletConfig getServletConfig() {
		return config;
	}

	@Override
	public String getServletInfo() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void init(ServletConfig config) throws ServletException {
		this.config = config;
		logger.info("Cart Controller loaded.");
	}

	@Override
	public void service(ServletRequest req, ServletResponse res) throws ServletException, IOException {
		res.setContentType("text/html");
		res.setCharacterEncoding("utf-8");
		PrintWriter out = res.getWriter();
		
		String mode = req.getParameter("mode");
		String username = req.getParameter("user");
		String bookID = req.getParameter("bookid");
		String quantity = req.getParameter("quan");
		
		if(username == null){
			out.print(ErrorMessage.msg_WrongParam);
			return;
		}
		
		switch(mode){
		case "add":
			if(bookID == null || quantity == null){
				out.print(ErrorMessage.msg_WrongParam);
				return;
			}
			
			int quan = 0, book = 0;
			try{
				quan = Integer.parseInt(quantity);
				book = Integer.parseInt(bookID);
			}catch(NumberFormatException e){
				logger.info("Parameter invalid");
				out.print(ErrorMessage.msg_WrongParam);
				return;
			}
			
			out.print(add(username, book, quan));
			break;
		case "clear":
			out.print(clear(username));
			break;
		default:
			out.print(ErrorMessage.msg_BadRequest);
		}
	}
	
	/**
	 * Add book(s) to user's cart. The quantity can be negative
	 * @param user
	 * @param bookID
	 * @param quantity
	 * @return process result
	 */
	private String add(String user, int bookID, int quantity){
		logger.info("adding new item to cart: " + bookID + " " + quantity);
		SessionFactory sessionFactory = HibernateUtils.getSessionFactory();
		Session session = sessionFactory.getCurrentSession();
		Transaction tx = session.beginTransaction();
		String hql = "FROM Cart WHERE Username = :username AND BookID = :id";
		Query query = session.createQuery(hql)
				.setParameter("username", user)
				.setParameter("id", bookID);
		@SuppressWarnings("unchecked")
		List<Cart> lst = query.list();
		if(lst.size() > 0){
			logger.info("update existing item");
			Cart cart = lst.get(0);
			cart.setQuantity(cart.getQuantity() + quantity);
			session.update(cart);
			tx.commit();
			return ErrorMessage.msg_Updated;
		}else{
			logger.info("add new item to cart");
			// did not have, so add new
			// check user exist
			Query quser = session.createQuery("FROM User WHERE Username = :username")
					.setParameter("username", user);
			if (quser.list().size() == 0){
				return ErrorMessage.msg_NotExist;
			}
			//
			Cart cart = new Cart();
			cart.setUsername(user);
			cart.setBookID(bookID);
			cart.setQuantity(quantity);
			session.save(cart);
			tx.commit();
			return ErrorMessage.msg_Added;
		}
	}
	
	private String clear(String username){
		logger.info("Clearing cart...");
		SessionFactory sessionFactory = HibernateUtils.getSessionFactory();
		Session session = sessionFactory.getCurrentSession();
		Transaction tx = session.beginTransaction();
		String hql = "DELETE FROM Cart WHERE Username = :username";
		Query query = session.createQuery(hql)
				.setParameter("username", username);
		query.executeUpdate();
		tx.commit();
		return ErrorMessage.msg_Cleared;
	}
}
