package ltm.dao;

import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import ltm.model.Category;
import ltm.utils.HibernateUtils;

public class CategoryDAO {
	@SuppressWarnings("unchecked")
	public List<Category> getCategories(){
		SessionFactory sessionFactory = HibernateUtils.getSessionFactory();
		Session session = sessionFactory.getCurrentSession();
		Transaction tx = session.beginTransaction();
		Query q = session.createQuery("FROM Category ORDER BY ID");
		List<Category> lst = q.list();
		tx.commit();
		return lst;
	}
	
	public boolean updateCategory(Category catg){
		if (catg == null){
			return false;
		}
		
		boolean result = false;
		SessionFactory sessionFactory = HibernateUtils.getSessionFactory();
		Session session = sessionFactory.getCurrentSession();
		Transaction tx = session.beginTransaction();
		Query q = session.createQuery("FROM Category WHERE ID = :id")
				.setParameter("id", catg.getID());
		Category ctg = (Category)q.uniqueResult();
		if(ctg != null){
			ctg.setName(catg.getName());
			session.update(ctg);
			result = true;
		}else{
			result = false;
		}
		tx.commit();
		return result;
	}
	
	public boolean removeCategory(Category catg){
		if(catg == null) return false;
		SessionFactory sessionFactory = HibernateUtils.getSessionFactory();
		Session session = sessionFactory.getCurrentSession();
		Transaction tx = session.beginTransaction();
		Query q = session.createQuery("DELETE FROM Category WHERE ID = :id")
				.setParameter("id", catg.getID());
		int result = q.executeUpdate();
		tx.commit();
		if(result == 1)
			return true;
		else
			return false;
	}
	
	public int addCategory(Category catg){
		if (catg == null){
			return -1;
		}
		
		List<Category> lst = getCategories();
		int newCatgID = 1;
		for(int i = 0; i < lst.size(); i++){
			if(lst.get(i).getID() != newCatgID)
				break;
			newCatgID++;
		}
		
		SessionFactory sessionFactory = HibernateUtils.getSessionFactory();
		Session session = sessionFactory.getCurrentSession();
		Transaction tx = session.beginTransaction();
		
		Query q = session.createQuery("FROM Category WHERE ID = :id")
				.setParameter("id", catg.getID());
		Category cbo = (Category)q.uniqueResult();
		if(cbo != null){
			newCatgID = -1;
		}else{
			cbo = new Category();
			cbo.setID(newCatgID);
			cbo.setName(catg.getName());
			try{
				session.save(cbo);
				tx.commit();
			}catch(HibernateException e){
				newCatgID = -1;
			}			
		}
		catg.setID(newCatgID);
		return newCatgID;
	}
}
