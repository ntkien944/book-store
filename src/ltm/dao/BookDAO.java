package ltm.dao;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import ltm.model.Book;
import ltm.utils.HibernateUtils;

public class BookDAO {
	@SuppressWarnings("unchecked")
	public List<Book> getBooks(){
		SessionFactory sessionFactory = HibernateUtils.getSessionFactory();
		Session session = sessionFactory.getCurrentSession();
		Transaction tx = session.beginTransaction();
		Query q = session.createQuery("FROM Book ORDER BY BookID");
		List<Book> lst = q.list();
		tx.commit();
		return lst;
	}
	
	public boolean updateBook(Book book){
		if (book == null){
			return false;
		}
		
		boolean result = false;
		SessionFactory sessionFactory = HibernateUtils.getSessionFactory();
		Session session = sessionFactory.getCurrentSession();
		Transaction tx = session.beginTransaction();
		Query q = session.createQuery("FROM Book WHERE BookID = :bookid")
				.setParameter("bookid", book.getBookID());
		Book cbo = (Book)q.uniqueResult();
		if(cbo != null){
			cbo.setName(book.getName());
			cbo.setAuthor(book.getAuthor());
			cbo.setPublisher(book.getPublisher());
			cbo.setPrice(book.getPrice());
			cbo.setCategory(book.getCategory());
			session.update(cbo);
			result = true;
		}else{
			result = false;
		}
		tx.commit();
		return result;
	}
	
	public boolean removeBook(Book book){
		if(book == null) return false;
		SessionFactory sessionFactory = HibernateUtils.getSessionFactory();
		Session session = sessionFactory.getCurrentSession();
		Transaction tx = session.beginTransaction();
		Query q = session.createQuery("DELETE FROM Book WHERE BookID = :bookid")
				.setParameter("bookid", book.getBookID());
		int result = q.executeUpdate();
		tx.commit();
		if(result == 1)
			return true;
		else
			return false;
	}
	
	public int addBook(Book book){
		if (book == null){
			return -1;
		}
		
		SessionFactory sessionFactory = HibernateUtils.getSessionFactory();
		Session session = sessionFactory.getCurrentSession();
		Transaction tx = session.beginTransaction();
		Query q = session.createQuery("SELECT MAX(BookID) FROM Book");
		int newID = -1;
		if (q.uniqueResult() != null)
			newID = (Integer)q.uniqueResult();
		newID++;
		q = session.createQuery("FROM Book WHERE BookID = :bookid")
				.setParameter("bookid", book.getBookID());
		Book cbo = (Book)q.uniqueResult();
		if(cbo != null){
			return -1;
		}else{
			cbo = new Book();
			cbo.setBookID(newID);
			cbo.setName(book.getName());
			cbo.setAuthor(book.getAuthor());
			cbo.setPublisher(book.getPublisher());
			cbo.setPrice(book.getPrice());
			cbo.setCategory(book.getCategory());
			session.save(cbo);
			tx.commit();
			return newID;
		}
	}
}
