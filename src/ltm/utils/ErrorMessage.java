package ltm.utils;

public class ErrorMessage {
	public static final String msg_WrongParam = "0 - wrong param.";
	public static final String msg_ConnectionFailed = "0 - connection failed.";
	public static final String msg_NotExist = "0 - not exists.";
	public static final String msg_UserExist = "0 - user already exists.";
	public static final String msg_BadRequest = "0 - bad request.";
	public static final String msg_Failed = "0 - failed.";
	public static final String msg_PasvEmpty = "0 - new password can not be empty.";
	public static final String msg_PasvValidation = "0 - old password do not match.";
	
	public static final String msg_Updated = "1 - updated.";
	public static final String msg_Added = "1 - added.";
	public static final String msg_Cleared = "1 - cleared.";
	public static final String msg_Success = "1 - success.";
	
	public static final String msg_OK = "OK";
	public static final String msg_FAIL = "FAILED";
}
