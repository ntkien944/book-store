package ltm.utils;

import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class HibernateUtils {
	
	private static SessionFactory sessionFactory = null;
	private static final Logger logger = LoggerFactory.getLogger(HibernateUtils.class);

	private static SessionFactory buildSessionFactory() {
		try {
			// logger.info("Build sessionFactory.");
			return new Configuration().configure().buildSessionFactory();
		} catch (Throwable ex) {
			logger.error("Initial SessionFactory creation failed." + ex);
			throw new ExceptionInInitializerError(ex);
		}
	}

	public static SessionFactory getSessionFactory() {
		if(sessionFactory == null)
			sessionFactory = buildSessionFactory();
		return sessionFactory;
	}
}
